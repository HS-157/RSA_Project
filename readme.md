# RSA Project

## Description

A RSA project for generate keys, crypt and decrypt message.

Add other algorithm are present in this project


## How use ?

We use this project with make, before, there are many soft installed for use.

### Need

You need :

- Python3
- GmPy
- Pydoc
- Make
- GCC

---

# Makefile

In this makefile, there are many commande :

- all
- doc
- test
- clear
- rsa
- mod_exp

### All

~~~bash
make all
~~~

Print different command.

## Doc

~~~bash
make doc
~~~

Generate a documention project in ```doc/```.

## Test

~~~bash
make test
~~~

Luch all test project.

## Clear

~~~bash
make clear
~~~

Clear project, remove ```doc``` and ```lib``` folder.

---

## RSA

This section use different command, as :

- rsa
- rsa_doc
- rsa_test

### RSA

~~~bash
make rsa
~~~

Lunch a rsa program for crypt / decrypt message.

We are a ```rsa``` python object, with differents methods and attributes.

On lunch, we are a message and keys, we can directly crypt message :

~~~python
rsa.crypt_msg()
~~~

We see a crypt message, we can get with ```rsa.msg_crypt```.

For decrypt :

~~~python
rsa.decrypt_msg()
~~~

A message is decrypt and we can get with ```rsa.msg_dcrpt```.

### Improve and rewrite crypt / decrypt fonction

<details>
  <summary>We can use differente fonction for other implementation of modular exponentiation</summary>
For fonction use Python implementation :

~~~python
rsa.crypt_msg_improve()
rsa.decrypt_msg_improve()
~~~

For fonction I write from [Wikipedia](https://en.wikipedia.org/wiki/Modular_exponentiation#Memory-efficient_method) :

~~~python
rsa.crypt_msg_rewrite()
rsa.decrypt_msg_rewrite()
~~~

Fonction write in C.

~~~python
rsa.crypt_msg_rewrite2()
rsa.decrypt_msg_rewrite2()
~~~

[Right-to-left binary method](https://en.wikipedia.org/wiki/Modular_exponentiation#Right-to-left_binary_method) write in Python.

~~~python
rsa.crypt_msg_rewrite3()
rsa.decrypt_msg_rewrite3()
~~~

[Right-to-left binary method](https://en.wikipedia.org/wiki/Modular_exponentiation#Right-to-left_binary_method) write in C.

~~~python
rsa.crypt_msg_rewrite4()
rsa.decrypt_msg_rewrite4()
~~~

[Left-to-right binary method](https://en.wikipedia.org/wiki/Modular_exponentiation#Left-to-right_binary_method).

~~~python
rsa.crypt_msg_rewrite5()
rsa.decrypt_msg_rewrite5()
~~~

[Right-to-left binary method](https://en.wikipedia.org/wiki/Modular_exponentiation#Right-to-left_binary_method) with exponentiation by squaring.

~~~python
rsa.crypt_msg_rewrite6()
rsa.decrypt_msg_rewrite6()
~~~

[Left-to-right binary method](https://en.wikipedia.org/wiki/Modular_exponentiation#Left-to-right_binary_method) with exponentiation by squaring.

~~~python
rsa.crypt_msg_rewrite7()
rsa.decrypt_msg_rewrite7()
~~~

[Right-to-left binary method](https://en.wikipedia.org/wiki/Modular_exponentiation#Right-to-left_binary_method) with exponentiation by squaring quaternary method.

~~~python
rsa.crypt_msg_rewrite8()
rsa.decrypt_msg_rewrite8()
~~~

[Left-to-right binary method](https://en.wikipedia.org/wiki/Modular_exponentiation#Left-to-right_binary_method) with exponentiation by squaring quaternary method.

~~~python
rsa.crypt_msg_rewrite9()
rsa.decrypt_msg_rewrite9()
~~~

[Right-to-left binary method](https://en.wikipedia.org/wiki/Modular_exponentiation#Right-to-left_binary_method) with exponentiation by squaring octal method.

~~~python
rsa.crypt_msg_rewrite10()
rsa.decrypt_msg_rewrite10()
~~~

[Left-to-right binary method](https://en.wikipedia.org/wiki/Modular_exponentiation#Left-to-right_binary_method) with exponentiation by squaring octal method.

~~~python
rsa.crypt_msg_rewrite11()
rsa.decrypt_msg_rewrite11()
~~~
</details>

---

We can change message with ```rsa.msg_strng``` :

~~~python
rsa.msg_strng = "Covfefe, Covfefe ! I love Covfefe !"
~~~

We can get a new RSA keys :

~~~python
rsa.get_keys()
~~~

And change size keys with paramater

~~~python
rsa.get_keys(15468,62233)
~~~

We can see a RSA keys with :

- Keys : ```rsa.keys```
- Public Key : ```rsa.public_key```
- Private Key : ```rsa.private_key```


### RSA_doc

~~~bash
make rsa_doc
~~~

Generate a documentation from python file in ```doc/rsa```

### RSA_test

~~~bash
make rsa_test
~~~

Lunch rsa test program.

---

## Modular exponentiation

This section use different command, as :

- mod_exp
- mod_exp_doc
- mod_exp_test
- mod_exp_build

### Mod_exp

~~~bash
make mod_exp
~~~

Lunch a python with modular exponentiation.

We use as :

~~~python
mod_expo(45,958,43)
mod_expo_c(45,958,43)
rtlbm(45,958,43)
rtlbm_c(45,958,43)
ltrbm(45,958,43)
rtlbm_ary(45,958,43)
ltrbm_ary(45,958,43)
rtlbm_quater(45,958,43)
ltrbm_quater(45,958,43)
rtlbm_octal(45,958,43)
ltrbm_octal(45,958,43)
~~~

### Mod_exp_doc

~~~bash
make mod_exp_doc
~~~

Generate a documentation from python file in ```doc/mod_exp```


### Mod_exp_test

~~~bash
make mod_exp_test
~~~

Lunch modular exponentiation test program.

### Mod_exp_build

~~~bash
make mod_exp_build
~~~

Compile a C program in lib use by Python.
