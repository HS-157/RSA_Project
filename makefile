all:
	echo 'Read README !'

test: mod_exp_test rsa_test

doc: rsa_doc mod_exp_doc

clear:
	if [ -d "doc" ]; then rm -r doc; fi
	if [ -d "lib" ]; then rm -r lib; fi
	#find . -iname "*.html" -exec rm -v {} \;

rsa: mod_exp_build src/rsa/*.py
	python3 -i src/rsa/main.py

rsa_doc:
	mkdir -p doc/rsa
	find src/rsa/ -iname "*.py" -exec pydoc3 -w {} \;
	mv *.html doc/rsa

rsa_test: mod_exp_build test/rsa/test_rsa.py
	python3 test/rsa/test_rsa.py

mod_exp: mod_exp_build src/modular_exponentiation.py
	python3 -i src/modular_exponentiation.py

mod_exp_doc: src/modular_exponentiation.py
	mkdir -p doc/mod_exp
	pydoc3 -w src/modular_exponentiation.py
	mv *.html doc/mod_exp

mod_exp_test: mod_exp_build test/modular_exponentiation/test_modular_exponentiation.py
	python3 test/modular_exponentiation/test_modular_exponentiation.py

mod_exp_build: clear src/C/modular_exponentiation.c
	mkdir lib
	gcc -shared -Wl,-soname,modular_exponentiation -o lib/modular_exponentiation.so -fPIC src/C/modular_exponentiation.c
