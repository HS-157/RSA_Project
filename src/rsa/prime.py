"""
Fonction use for prime operation?
"""
from random import randrange
from math import gcd
from gmpy2 import next_prime

def prime_with(number):
    """
    Return a prime number with number.
    :param number: A number
    :type number: int
    :return: Return a number who prime with number
    :rtype: int

    :Example:
    >>> prime_with(128)
    87
    """
    prime = randrange(2, number)
    is_prime = False

    while (is_prime == False or prime >= number):
        b = gcd(prime, number)
        if b == 1:
            is_prime = True
            break
        prime += 1

    if prime >= number:
        prime_with(number)
    return prime


def prime(number):
    """
    Return a prime number next to number.
    :param number: A number
    :type number: int
    :return: Return a prime number
    :rtype: int

    :Example:
    >>> prime(250)
    251
    """
    prime = next_prime(number)
    return int(prime)
