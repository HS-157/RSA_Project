"""
Fonction use for convert a message between string and ascii formw
"""
def str2ascii(msg_str):
    """
    Return a ascii message from string message.
    :param msg_str: A string message in the form of an string
    :type msg_str: str
    :return: Return a ascii message in the form of an integer list
    :rtype: list(int)

    :Example:
    >>> str2ascii('Bonjour')
    [66, 111, 110, 106, 111, 117, 114]
    """
    msg_ascii = []
    for i in msg_str:
        msg_ascii.append(ord(i))
    return msg_ascii

def ascii2str(msg_ascii):
    """
    Return string message from ascii message.
    :param msg_ascii: A ascii message in the form of an integer list
    :type msg_ascii: int
    :return: Return a string message in the form of an string
    :rtype: str

    :Example:
    >>> ascii2str([66, 111, 110, 106, 111, 117, 114])
    'Bonjour'
    """
    msg = ""
    for i in msg_ascii:
        msg += chr(i)
    return msg
