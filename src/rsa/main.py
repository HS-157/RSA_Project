"""
A main file for rsa demonstration.
"""

try:
    from convert import str2ascii, ascii2str
    from rsa import crypt, decrypt, keys, crypt_improve, decrypt_improve, crypt_rewrite, decrypt_rewrite, crypt_rewrite2, decrypt_rewrite2, crypt_rewrite3, decrypt_rewrite3, crypt_rewrite4, decrypt_rewrite4, crypt_rewrite5, decrypt_rewrite5, crypt_rewrite6, decrypt_rewrite6, crypt_rewrite7, decrypt_rewrite7, crypt_rewrite8, decrypt_rewrite8, crypt_rewrite9, decrypt_rewrite9, crypt_rewrite10, decrypt_rewrite10, crypt_rewrite11, decrypt_rewrite11

except ImportError as e:
    from .convert import str2ascii, ascii2str
    from .rsa import crypt, decrypt, keys, crypt_improve, decrypt_improve, crypt_rewrite, decrypt_rewrite, crypt_rewrite2, decrypt_rewrite2, crypt_rewrite3, decrypt_rewrite3, crypt_rewrite4, decrypt_rewrite4, crypt_rewrite5, decrypt_rewrite5, crypt_rewrite6, decrypt_rewrite6, crypt_rewrite7, decrypt_rewrite7, crypt_rewrite8, decrypt_rewrite8, crypt_rewrite9, decrypt_rewrite9, crypt_rewrite10, decrypt_rewrite10, crypt_rewrite11, decrypt_rewrite11

from functools import partial
from random import randrange

class rsa:
    """
    A rsa object with methode for crypt and decrypt message and get rsa keys.
    """
    def __init__(self):
        """
        A init methode.
        """
        self.keys = keys()
        self.public_key = self.keys[0]
        self.private_key = self.keys[1]
        self.msg_strng = "Hello World"
        #print("Keys :", self.keys)

    def crypt_msg(self):
        """
        A methode for crypt message from self.msg_strng.
        """
        msg_ascii = str2ascii(self.msg_strng)
        self.msg_crypt = crypt(msg_ascii, self.public_key)
        #print(self.msg_crypt)
    
    def decrypt_msg(self):
        """
        A methode for decrypt message from self.msg_crypt.
        """
        msg_decrypt = decrypt(self.msg_crypt, self.private_key)
        self.msg_dcrpt = ascii2str(msg_decrypt)
        #print(self.msg_dcrpt)

    def get_keys(self, p = None, q = None):
        """
        A methode for get a new keys
        """
        if p is None:
            p = randrange(100, 500)
        if q is None:
            q = randrange(100, 500)
            while(q == p):
                q = randrange(100, 500)
        self.keys = keys(p,q)
        self.public_key = self.keys[0]
        self.private_key = self.keys[1]
        #print("Keys :", self.keys)

    def crypt_msg_improve(self):
        """
        A methode improve for crypt message from self.msg_strng.
        """
        msg_ascii = str2ascii(self.msg_strng)
        self.msg_crypt = crypt_improve(msg_ascii, self.public_key)
        #print(self.msg_crypt)
    
    def decrypt_msg_improve(self):
        """
        A methode improve for decrypt message from self.msg_crypt.
        """
        msg_decrypt = decrypt_improve(self.msg_crypt, self.private_key)
        self.msg_dcrpt = ascii2str(msg_decrypt)
        #print(self.msg_dcrpt)

    def crypt_msg_rewrite(self):
        """
        A methode improve for crypt message from self.msg_strng.
        """
        msg_ascii = str2ascii(self.msg_strng)
        self.msg_crypt = crypt_rewrite(msg_ascii, self.public_key)
        #print(self.msg_crypt)
    
    def decrypt_msg_rewrite(self):
        """
        A methode improve for decrypt message from self.msg_crypt.
        """
        msg_decrypt = decrypt_rewrite(self.msg_crypt, self.private_key)
        self.msg_dcrpt = ascii2str(msg_decrypt)
        #print(self.msg_dcrpt)


def add_crypt_msg(cls,i):
    """
    Fonction for generate a crypt_msg fonction
    """
    def in_crypt_msg(self):
        """
        A methode improve for crypt message from self.msg_strng.
        """
        msg_ascii = str2ascii(self.msg_strng)
        self.msg_crypt = eval("crypt_rewrite%d" % i)(msg_ascii, self.public_key)
        #print(self.msg_crypt)
    in_crypt_msg.__name__ = "crypt_msg_rewrite%d" % i
    setattr(cls,in_crypt_msg.__name__,in_crypt_msg)

def add_decrypt_msg(cls,i):
    """
    Fonction for generate a decrypt_msg fonction
    """
    def in_decrypt_msg(self):
        """
        A methode improve for decrypt message from self.msg_crypt.
        """
        msg_decrypt = eval("decrypt_rewrite%d" % i)(self.msg_crypt, self.private_key)
        self.msg_dcrpt = ascii2str(msg_decrypt)
        #print(self.msg_dcrpt)
    in_decrypt_msg.__name__ = "decrypt_msg_rewrite%d" % i
    setattr(cls,in_decrypt_msg.__name__,in_decrypt_msg)

for i in range(2,12):
    add_crypt_msg(rsa, i)
    add_decrypt_msg(rsa, i)

if __name__ == '__main__':
    """
    A main "fonction" for lunch a demonstration
    """
    keys_p = keys()
    public_key = keys_p[0]
    private_key = keys_p[1]

    msg = "Bonjour"
    #print("msg :", msg)

    msg_ascii = str2ascii(msg)

    msg_crypt = crypt(msg_ascii, public_key)
    #print("msg_crypt :", msg_crypt)

    msg_decrypt = decrypt(msg_crypt, private_key)
    #print("msg_decrypt :", msg_decrypt)

    msg_str = ascii2str(msg_decrypt)
    #print("msg_s :", msg_str)

    rsa = rsa()
