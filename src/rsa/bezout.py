"""
Fonction for Bézout's fonction
"""

def bezout(c, m):
    """
    Return Bézout's identity.

    https://fr.wikipedia.org/wiki/Algorithme_d%27Euclide_%C3%A9tendu
    """
    u = xgcd(c,m)[1]

    if u < 0:
        k = 1
        u_0 = u
        while u < 2 or u > m:
            u = u_0 - k * m
            k -= 1
    return u

def xgcd(c, m):
    """
    A extended Euclidean algorithm

    Use from wikibooks :
    - https://en.wikibooks.org/wiki/Algorithm_Implementation/Mathematics/Extended_Euclidean_algorithm#Iterative_algorithm
    For more information, read this link
    """
    x0, x1, y0, y1 = 1, 0, 0, 1
    while m != 0:
        q, c, m = c // m, m, c % m
        x0, x1 = x1, x0 - q * x1
        y0, y1 = y1, y0 - q * y1
    return  c, x0, y0
