"""
RSA fonction use for crypt and decrypt message
"""
try:
    from prime import prime_with, prime
    from bezout import bezout
    from modular_exponentiation import mod_expo, mod_expo_c, rtlbm, rtlbm_c, ltrbm, rtlbm_ary, ltrbm_ary, rtlbm_quater, ltrbm_quater, rtlbm_octal, ltrbm_octal
except ImportError as e:
    from .prime import prime_with, prime
    from .bezout import bezout
    from .modular_exponentiation import mod_expo, mod_expo_c, rtlbm, rtlbm_c, ltrbm, rtlbm_ary, ltrbm_ary, rtlbm_quater, ltrbm_quater, rtlbm_octal, ltrbm_octal

from random import randrange

def crypt(msg_ascii, public_key):
    """
    Return a crypt message.
    :param msg_ascii: A message in the form of an integer list
    :type msg_ascii: list(int)
    :param public_key: A public key in the form an interger tuple
    :type public_key: tuple(int)
    :return: Return a encrypt message in the form of an integer list
    :rtype: list(int)

    :Example:
    >>> crypt([66, 111, 110, 106, 111, 117, 114], (45069, 47083))
    [45748, 22573, 22587, 12128, 22573, 3438, 40686]
    """
    msg_crypt = []
    for i in msg_ascii:
        msg_crypt.append(i ** public_key[0] % public_key[1])
    return msg_crypt

def decrypt(msg_crypt, private_key):
    """
    Return a decrypt message.
    :param msg_crypt: A encrypt message in the form of an integer list
    :type msg_crypt: list(int)
    :param private_key: A private key in the form an interger tuple
    :type private_key: tuple(int)
    :return: Return a decrypt message in the form of an integer list
    :rtype: list(int)

    :Example:
    >>> decrypt([45748, 22573, 22587, 12128, 22573, 3438, 40686], (38701, 47083))
    [66, 111, 110, 106, 111, 117, 114]
    """
    msg_decrypt = []
    for i in msg_crypt:
        msg_decrypt.append(i ** private_key[0] % private_key[1])
    return msg_decrypt

def keys(p_number = randrange(100,1000), q_number = randrange(100,1000)):
    """
    Return pair rsa keys.
    :param p_number: A number who going a prime number
    :type p_number: int
    :param q_number: A number who going a prime number
    :type q_number: int
    :return: Return a pair rsa key in the form of an integer tuple tuple
    :rtype: list(int)

    :Example:
    >>> keys(499, 389)
    ((148901, 194111), (81173, 194111))

    .. todo:: If p_number and q_number is bigger, the crypt and decrypt fontion beween long time, fixe this
    """
    while(p_number == q_number):
        p_number = randrange(100,500)
        q_number = randrange(100,500)

    p = prime(p_number)
    q = prime(q_number)

    n = p * q
    m = (p - 1) * (q - 1)
    c = prime_with(m)
    u = bezout(c, m)
    return ((c, n), (u, n))

def crypt_improve(msg_ascii, public_key):
    """
    Return a crypt message.
    :param msg_ascii: A message in the form of an integer list
    :type msg_ascii: list(int)
    :param public_key: A public key in the form an interger tuple
    :type public_key: tuple(int)
    :return: Return a encrypt message in the form of an integer list
    :rtype: list(int)

    :Example:
    >>> crypt_improve([66, 111, 110, 106, 111, 117, 114], (45069, 47083))
    [45748, 22573, 22587, 12128, 22573, 3438, 40686]
    """
    msg_crypt = []
    for i in msg_ascii:
        msg_crypt.append(pow(i, public_key[0], public_key[1]))
    return msg_crypt

def decrypt_improve(msg_crypt, private_key):
    """
    Return a decrypt message.
    :param msg_crypt: A encrypt message in the form of an integer list
    :type msg_crypt: list(int)
    :param private_key: A private key in the form an interger tuple
    :type private_key: tuple(int)
    :return: Return a decrypt message in the form of an integer list
    :rtype: list(int)

    :Example:
    >>> decrypt_improve([45748, 22573, 22587, 12128, 22573, 3438, 40686], (38701, 47083))
    [66, 111, 110, 106, 111, 117, 114]
    """
    msg_decrypt = []
    for i in msg_crypt:
        msg_decrypt.append(pow(i, private_key[0], private_key[1]))
    return msg_decrypt

def crypt_rewrite(msg_ascii, public_key):
    """
    Return a crypt message.
    :param msg_ascii: A message in the form of an integer list
    :type msg_ascii: list(int)
    :param public_key: A public key in the form an interger tuple
    :type public_key: tuple(int)
    :return: Return a encrypt message in the form of an integer list
    :rtype: list(int)

    :Example:
    >>> crypt_rewrite([66, 111, 110, 106, 111, 117, 114], (45069, 47083))
    [45748, 22573, 22587, 12128, 22573, 3438, 40686]
    """
    msg_crypt = []
    for i in msg_ascii:
        msg_crypt.append(mod_expo(i, public_key[0], public_key[1]))
    return msg_crypt

def decrypt_rewrite(msg_crypt, private_key):
    """
    Return a decrypt message.
    :param msg_crypt: A encrypt message in the form of an integer list
    :type msg_crypt: list(int)
    :param private_key: A private key in the form an interger tuple
    :type private_key: tuple(int)
    :return: Return a decrypt message in the form of an integer list
    :rtype: list(int)

    :Example:
    >>> decrypt_rewrite([45748, 22573, 22587, 12128, 22573, 3438, 40686], (38701, 47083))
    [66, 111, 110, 106, 111, 117, 114]
    """
    msg_decrypt = []
    for i in msg_crypt:
        msg_decrypt.append(mod_expo(i, private_key[0], private_key[1]))
    return msg_decrypt

def crypt_rewrite2(msg_ascii, public_key):
    """
    Return a crypt message.
    :param msg_ascii: A message in the form of an integer list
    :type msg_ascii: list(int)
    :param public_key: A public key in the form an interger tuple
    :type public_key: tuple(int)
    :return: Return a encrypt message in the form of an integer list
    :rtype: list(int)

    :Example:
    >>> crypt_rewrite2([66, 111, 110, 106, 111, 117, 114], (45069, 47083))
    [45748, 22573, 22587, 12128, 22573, 3438, 40686]
    """
    msg_crypt = []
    for i in msg_ascii:
        msg_crypt.append(mod_expo_c(i, public_key[0], public_key[1]))
    return msg_crypt

def decrypt_rewrite2(msg_crypt, private_key):
    """
    Return a decrypt message.
    :param msg_crypt: A encrypt message in the form of an integer list
    :type msg_crypt: list(int)
    :param private_key: A private key in the form an interger tuple
    :type private_key: tuple(int)
    :return: Return a decrypt message in the form of an integer list
    :rtype: list(int)

    :Example:
    >>> decrypt_rewrite2([45748, 22573, 22587, 12128, 22573, 3438, 40686], (38701, 47083))
    [66, 111, 110, 106, 111, 117, 114]
    """
    msg_decrypt = []
    for i in msg_crypt:
        msg_decrypt.append(mod_expo_c(i, private_key[0], private_key[1]))
    return msg_decrypt

def crypt_rewrite3(msg_ascii, public_key):
    """
    Return a crypt message.
    :param msg_ascii: A message in the form of an integer list
    :type msg_ascii: list(int)
    :param public_key: A public key in the form an interger tuple
    :type public_key: tuple(int)
    :return: Return a encrypt message in the form of an integer list
    :rtype: list(int)

    :Example:
    >>> crypt_rewrite3([66, 111, 110, 106, 111, 117, 114], (45069, 47083))
    [45748, 22573, 22587, 12128, 22573, 3438, 40686]
    """
    msg_crypt = []
    for i in msg_ascii:
        msg_crypt.append(rtlbm(i, public_key[0], public_key[1]))
    return msg_crypt

def decrypt_rewrite3(msg_crypt, private_key):
    """
    Return a decrypt message.
    :param msg_crypt: A encrypt message in the form of an integer list
    :type msg_crypt: list(int)
    :param private_key: A private key in the form an interger tuple
    :type private_key: tuple(int)
    :return: Return a decrypt message in the form of an integer list
    :rtype: list(int)

    :Example:
    >>> decrypt_rewrite3([45748, 22573, 22587, 12128, 22573, 3438, 40686], (38701, 47083))
    [66, 111, 110, 106, 111, 117, 114]
    """
    msg_decrypt = []
    for i in msg_crypt:
        msg_decrypt.append(rtlbm(i, private_key[0], private_key[1]))
    return msg_decrypt

def crypt_rewrite4(msg_ascii, public_key):
    """
    Return a crypt message.
    :param msg_ascii: A message in the form of an integer list
    :type msg_ascii: list(int)
    :param public_key: A public key in the form an interger tuple
    :type public_key: tuple(int)
    :return: Return a encrypt message in the form of an integer list
    :rtype: list(int)

    :Example:
    >>> crypt_rewrite4([66, 111, 110, 106, 111, 117, 114], (45069, 47083))
    [45748, 22573, 22587, 12128, 22573, 3438, 40686]
    """
    msg_crypt = []
    for i in msg_ascii:
        msg_crypt.append(rtlbm_c(i, public_key[0], public_key[1]))
    return msg_crypt

def decrypt_rewrite4(msg_crypt, private_key):
    """
    Return a decrypt message.
    :param msg_crypt: A encrypt message in the form of an integer list
    :type msg_crypt: list(int)
    :param private_key: A private key in the form an interger tuple
    :type private_key: tuple(int)
    :return: Return a decrypt message in the form of an integer list
    :rtype: list(int)

    :Example:
    >>> decrypt_rewrite4([45748, 22573, 22587, 12128, 22573, 3438, 40686], (38701, 47083))
    [66, 111, 110, 106, 111, 117, 114]
    """
    msg_decrypt = []
    for i in msg_crypt:
        msg_decrypt.append(rtlbm_c(i, private_key[0], private_key[1]))
    return msg_decrypt

def crypt_rewrite5(msg_ascii, public_key):
    """
    Return a crypt message.
    :param msg_ascii: A message in the form of an integer list
    :type msg_ascii: list(int)
    :param public_key: A public key in the form an interger tuple
    :type public_key: tuple(int)
    :return: Return a encrypt message in the form of an integer list
    :rtype: list(int)

    :Example:
    >>> crypt_rewrite5([66, 111, 110, 106, 111, 117, 114], (45069, 47083))
    [45748, 22573, 22587, 12128, 22573, 3438, 40686]
    """
    msg_crypt = []
    for i in msg_ascii:
        msg_crypt.append(ltrbm(i, public_key[0], public_key[1]))
    return msg_crypt

def decrypt_rewrite5(msg_crypt, private_key):
    """
    Return a decrypt message.
    :param msg_crypt: A encrypt message in the form of an integer list
    :type msg_crypt: list(int)
    :param private_key: A private key in the form an interger tuple
    :type private_key: tuple(int)
    :return: Return a decrypt message in the form of an integer list
    :rtype: list(int)

    :Example:
    >>> decrypt_rewrite5([45748, 22573, 22587, 12128, 22573, 3438, 40686], (38701, 47083))
    [66, 111, 110, 106, 111, 117, 114]
    """
    msg_decrypt = []
    for i in msg_crypt:
        msg_decrypt.append(ltrbm(i, private_key[0], private_key[1]))
    return msg_decrypt

def crypt_rewrite6(msg_ascii, public_key):
    """
    Return a crypt message.
    :param msg_ascii: A message in the form of an integer list
    :type msg_ascii: list(int)
    :param public_key: A public key in the form an interger tuple
    :type public_key: tuple(int)
    :return: Return a encrypt message in the form of an integer list
    :rtype: list(int)

    :Example:
    >>> crypt_rewrite7([66, 111, 110, 106, 111, 117, 114], (45069, 47083))
    [45748, 22573, 22587, 12128, 22573, 3438, 40686]
    """
    msg_crypt = []
    for i in msg_ascii:
        msg_crypt.append(rtlbm_ary(i, public_key[0], public_key[1]))
    return msg_crypt

def decrypt_rewrite6(msg_crypt, private_key):
    """
    Return a decrypt message.
    :param msg_crypt: A encrypt message in the form of an integer list
    :type msg_crypt: list(int)
    :param private_key: A private key in the form an interger tuple
    :type private_key: tuple(int)
    :return: Return a decrypt message in the form of an integer list
    :rtype: list(int)

    :Example:
    >>> decrypt_rewrite7([45748, 22573, 22587, 12128, 22573, 3438, 40686], (38701, 47083))
    [66, 111, 110, 106, 111, 117, 114]
    """
    msg_decrypt = []
    for i in msg_crypt:
        msg_decrypt.append(rtlbm_ary(i, private_key[0], private_key[1]))
    return msg_decrypt

def crypt_rewrite7(msg_ascii, public_key):
    """
    Return a crypt message.
    :param msg_ascii: A message in the form of an integer list
    :type msg_ascii: list(int)
    :param public_key: A public key in the form an interger tuple
    :type public_key: tuple(int)
    :return: Return a encrypt message in the form of an integer list
    :rtype: list(int)

    :Example:
    >>> crypt_rewrite8([66, 111, 110, 106, 111, 117, 114], (45069, 47083))
    [45748, 22573, 22587, 12128, 22573, 3438, 40686]
    """
    msg_crypt = []
    for i in msg_ascii:
        msg_crypt.append(ltrbm_ary(i, public_key[0], public_key[1]))
    return msg_crypt

def decrypt_rewrite7(msg_crypt, private_key):
    """
    Return a decrypt message.
    :param msg_crypt: A encrypt message in the form of an integer list
    :type msg_crypt: list(int)
    :param private_key: A private key in the form an interger tuple
    :type private_key: tuple(int)
    :return: Return a decrypt message in the form of an integer list
    :rtype: list(int)

    :Example:
    >>> decrypt_rewrite8([45748, 22573, 22587, 12128, 22573, 3438, 40686], (38701, 47083))
    [66, 111, 110, 106, 111, 117, 114]
    """
    msg_decrypt = []
    for i in msg_crypt:
        msg_decrypt.append(ltrbm_ary(i, private_key[0], private_key[1]))
    return msg_decrypt


def crypt_rewrite8(msg_ascii, public_key):
    """
    Return a crypt message.
    :param msg_ascii: A message in the form of an integer list
    :type msg_ascii: list(int)
    :param public_key: A public key in the form an interger tuple
    :type public_key: tuple(int)
    :return: Return a encrypt message in the form of an integer list
    :rtype: list(int)

    :Example:
    >>> crypt_rewrite8([66, 111, 110, 106, 111, 117, 114], (45069, 47083))
    [45748, 22573, 22587, 12128, 22573, 3438, 40686]
    """
    msg_crypt = []
    for i in msg_ascii:
        msg_crypt.append(rtlbm_quater(i, public_key[0], public_key[1]))
    return msg_crypt

def decrypt_rewrite8(msg_crypt, private_key):
    """
    Return a decrypt message.
    :param msg_crypt: A encrypt message in the form of an integer list
    :type msg_crypt: list(int)
    :param private_key: A private key in the form an interger tuple
    :type private_key: tuple(int)
    :return: Return a decrypt message in the form of an integer list
    :rtype: list(int)

    :Example:
    >>> decrypt_rewrite8([45748, 22573, 22587, 12128, 22573, 3438, 40686], (38701, 47083))
    [66, 111, 110, 106, 111, 117, 114]
    """
    msg_decrypt = []
    for i in msg_crypt:
        msg_decrypt.append(rtlbm_quater(i, private_key[0], private_key[1]))
    return msg_decrypt

def crypt_rewrite9(msg_ascii, public_key):
    """
    Return a crypt message.
    :param msg_ascii: A message in the form of an integer list
    :type msg_ascii: list(int)
    :param public_key: A public key in the form an interger tuple
    :type public_key: tuple(int)
    :return: Return a encrypt message in the form of an integer list
    :rtype: list(int)

    :Example:
    >>> crypt_rewrite9([66, 111, 110, 106, 111, 117, 114], (45069, 47083))
    [45748, 22573, 22587, 12128, 22573, 3438, 40686]
    """
    msg_crypt = []
    for i in msg_ascii:
        msg_crypt.append(ltrbm_quater(i, public_key[0], public_key[1]))
    return msg_crypt

def decrypt_rewrite9(msg_crypt, private_key):
    """
    Return a decrypt message.
    :param msg_crypt: A encrypt message in the form of an integer list
    :type msg_crypt: list(int)
    :param private_key: A private key in the form an interger tuple
    :type private_key: tuple(int)
    :return: Return a decrypt message in the form of an integer list
    :rtype: list(int)

    :Example:
    >>> decrypt_rewrite9([45748, 22573, 22587, 12128, 22573, 3438, 40686], (38701, 47083))
    [66, 111, 110, 106, 111, 117, 114]
    """
    msg_decrypt = []
    for i in msg_crypt:
        msg_decrypt.append(ltrbm_quater(i, private_key[0], private_key[1]))
    return msg_decrypt

def crypt_rewrite10(msg_ascii, public_key):
    """
    Return a crypt message.
    :param msg_ascii: A message in the form of an integer list
    :type msg_ascii: list(int)
    :param public_key: A public key in the form an interger tuple
    :type public_key: tuple(int)
    :return: Return a encrypt message in the form of an integer list
    :rtype: list(int)

    :Example:
    >>> crypt_rewrite10([66, 111, 110, 106, 111, 117, 114], (45069, 47083))
    [45748, 22573, 22587, 12128, 22573, 3438, 40686]
    """
    msg_crypt = []
    for i in msg_ascii:
        msg_crypt.append(rtlbm_octal(i, public_key[0], public_key[1]))
    return msg_crypt

def decrypt_rewrite10(msg_crypt, private_key):
    """
    Return a decrypt message.
    :param msg_crypt: A encrypt message in the form of an integer list
    :type msg_crypt: list(int)
    :param private_key: A private key in the form an interger tuple
    :type private_key: tuple(int)
    :return: Return a decrypt message in the form of an integer list
    :rtype: list(int)

    :Example:
    >>> decrypt_rewrite10([45748, 22573, 22587, 12128, 22573, 3438, 40686], (38701, 47083))
    [66, 111, 110, 106, 111, 117, 114]
    """
    msg_decrypt = []
    for i in msg_crypt:
        msg_decrypt.append(rtlbm_octal(i, private_key[0], private_key[1]))
    return msg_decrypt

def crypt_rewrite11(msg_ascii, public_key):
    """
    Return a crypt message.
    :param msg_ascii: A message in the form of an integer list
    :type msg_ascii: list(int)
    :param public_key: A public key in the form an interger tuple
    :type public_key: tuple(int)
    :return: Return a encrypt message in the form of an integer list
    :rtype: list(int)

    :Example:
    >>> crypt_rewrite11([66, 111, 110, 106, 111, 117, 114], (45069, 47083))
    [45748, 22573, 22587, 12128, 22573, 3438, 40686]
    """
    msg_crypt = []
    for i in msg_ascii:
        msg_crypt.append(ltrbm_octal(i, public_key[0], public_key[1]))
    return msg_crypt

def decrypt_rewrite11(msg_crypt, private_key):
    """
    Return a decrypt message.
    :param msg_crypt: A encrypt message in the form of an integer list
    :type msg_crypt: list(int)
    :param private_key: A private key in the form an interger tuple
    :type private_key: tuple(int)
    :return: Return a decrypt message in the form of an integer list
    :rtype: list(int)

    :Example:
    >>> decrypt_rewrite11([45748, 22573, 22587, 12128, 22573, 3438, 40686], (38701, 47083))
    [66, 111, 110, 106, 111, 117, 114]
    """
    msg_decrypt = []
    for i in msg_crypt:
        msg_decrypt.append(ltrbm_octal(i, private_key[0], private_key[1]))
    return msg_decrypt
