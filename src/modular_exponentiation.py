"""
Fonction for modular exponentiation from Wikipedia
https://en.wikipedia.org/wiki/Modular_exponentiation
"""
import ctypes, os
path = os.getcwd() + "/lib/modular_exponentiation.so"
c = ctypes.CDLL(path)

def mod_expo(b,e,m):
    """
    Return result for modular exponentiation.
    https://en.wikipedia.org/wiki/Modular_exponentiation#Memory-efficient_method
    :param b: A number
    :type b: int
    :param e: A number
    :type e: int
    :param m: A number
    :type m: int
    :return: Return a number who is result modular exponentiation
    :rtype: int

    :Example:
    >>> mod_expo(45,958,43)
    21
    """
    c = 1
    f = 0
    while(f < e):
        f += 1
        c = (b * c) % m
    return c

def mod_expo_c(b,e,m):
    """
    Return result for modular exponentiation implement in C.
    https://en.wikipedia.org/wiki/Modular_exponentiation#Memory-efficient_method
    :param b: A number
    :type b: int
    :param e: A number
    :type e: int
    :param m: A number
    :type m: int
    :return: Return a number who is result modular exponentiation
    :rtype: int

    :Example:
    >>> mod_expo_c(45,958,43)
    21
    """
    return c.expo_mod(b,e,m)

def rtlbm_c(b,e,m):
    """
    Return result for modular exponentiation binary implement in C.
    https://en.wikipedia.org/wiki/Modular_exponentiation#Memory-efficient_method
    :param b: A number
    :type b: int
    :param e: A number
    :type e: int
    :param m: A number
    :type m: int
    :return: Return a number who is result modular exponentiation
    :rtype: int

    :Example:
    >>> rtlbm_c(45,958,43)
    21
    """
    return c.rtlbm(b,e,m)


def rtlbm(b,e,m):
    """
    Return result for modular exponentiation binary.
    https://en.wikipedia.org/wiki/Modular_exponentiation#Right-to-left_binary_method
    :param b: A number
    :type b: int
    :param e: A number
    :type e: int
    :param m: A number
    :type m: int
    :return: Return a number who is result modular exponentiation
    :rtype: int

    :Example:
    >>> rtlbm(45,958,43)
    21
    """
    result = 1
    while e > 0:
        if e & 1 > 0:
            result = (result * b) % m
        e >>= 1
        b = (b * b) % m
    return result

def ltrbm(b,e,m):
    """
    Return result for modular exponentiation binary.
    https://en.wikipedia.org/wiki/Modular_exponentiation#Left-to-right_binary_method
    :param b: A number
    :type b: int
    :param e: A number
    :type e: int
    :param m: A number
    :type m: int
    :return: Return a number who is result modular exponentiation
    :rtype: int

    :Example:
    >>> ltrbm(45,958,43)
    21
    """
    def dec2bin(n):
        bits = []
        while n:
                bits.append(n & 1)
                n >>= 1
        return bits
    e_bin = dec2bin(e)
    result = 1
    e_bin.reverse()
    for i in e_bin:
        result = (result * result) % m
        if i == 1:
            result = result * b % m
    return result

def expo_fast(b,e):
    """
    Return result for exponentiation by squaring.
    https://en.wikipedia.org/wiki/Exponentiation_by_squaring
    :param b: A number
    :type b: int
    :param e: A number
    :type e: int
    :return: Return a number who is result exponentiation
    :rtype: int

    :Example:
    >>> expo_fast(45,13)
    3102863559971923828125
    """
    if e == 0:
        return 1
    elif e % 2 == 0:
        return expo_fast(b * b, e / 2)
    else:
        return b * expo_fast(b * b, (e - 1) / 2)

def rtlbm_ary(b,e,m):
    """
    Return result for modular exponentiation binary with exponentiation by squaring.
    https://en.wikipedia.org/wiki/Modular_exponentiation#Right-to-left_binary_method
    :param b: A number
    :type b: int
    :param e: A number
    :type e: int
    :param m: A number
    :type m: int
    :return: Return a number who is result modular exponentiation
    :rtype: int

    :Example:
    >>> rtlbm_ary(45,958,43)
    21
    """
    result = 1
    while e > 0:
        if e & 1 > 0:
            result = (result * b) % m
        e >>= 1
        b = expo_fast(b, 2) % m
    return result

def dec2bin(n):
    bits = []
    while n:
        bits.append(n & 1)
        n >>= 1
    return bits

def ltrbm_ary(b,e,m):
    """
    Return result for modular exponentiation binary with exponentiation by squaring.
    https://en.wikipedia.org/wiki/Modular_exponentiation#Left-to-right_binary_method
    :param b: A number
    :type b: int
    :param e: A number
    :type e: int
    :param m: A number
    :type m: int
    :return: Return a number who is result modular exponentiation
    :rtype: int

    :Example:
    >>> ltrbm_ary(45,958,43)
    21
    """
    e_bin = dec2bin(e)
    result = 1
    e_bin.reverse()
    for i in e_bin:
        result = expo_fast(result, 2) % m
        if i == 1:
            result = result * b % m
    return result

def expo_quater(b,e):
    e_bin = dec2bin(e)
    e_bin.reverse()
    if len(e_bin) % 2 == 1:
        e_bin = [0] + e_bin
    result = 0
    if e_bin[0] == 0 and e_bin[1] == 0:
        result = 1
    elif e_bin[0] == 0 and e_bin[1] == 1:
        result = b
    elif e_bin[0] == 1 and e_bin[1] == 0:
        result = b ** 2
    elif e_bin[0] == 1 and e_bin[1] == 1:
        result = b ** 3
    else:
        pass
    for i in range(2, len(e_bin), 2):
        if e_bin[i] == 0 and e_bin[i+1] == 0:
            result = result ** 4
        elif e_bin[i] == 0 and e_bin[i+1] == 1:
            result = result ** 4 * b
        elif e_bin[i] == 1 and e_bin[i+1] == 0:
            result = result ** 4 * (b ** 2)
        elif e_bin[i] == 1 and e_bin[i+1] == 1:
            result = result ** 4 * (b ** 3)
        else:
            pass
    return result


def rtlbm_quater(b,e,m):
    """
    Return result for modular exponentiation binary with exponentiation by squaring quaternary method.
    https://en.wikipedia.org/wiki/Modular_exponentiation#Right-to-left_binary_method
    :param b: A number
    :type b: int
    :param e: A number
    :type e: int
    :param m: A number
    :type m: int
    :return: Return a number who is result modular exponentiation
    :rtype: int

    :Example:
    >>> rtlbm_quater(45,958,43)
    21
    """
    result = 1
    while e > 0:
        if e & 1 > 0:
            result = (result * b) % m
        e >>= 1
        b = expo_quater(b, 2) % m
    return result

def ltrbm_quater(b,e,m):
    """
    Return result for modular exponentiation binary with exponentiation by squaring quaternary method.
    https://en.wikipedia.org/wiki/Modular_exponentiation#Left-to-right_binary_method
    :param b: A number
    :type b: int
    :param e: A number
    :type e: int
    :param m: A number
    :type m: int
    :return: Return a number who is result modular exponentiation
    :rtype: int

    :Example:
    >>> ltrbm_quater(45,958,43)
    21
    """
    e_bin = dec2bin(e)
    result = 1
    e_bin.reverse()
    for i in e_bin:
        result = expo_quater(result, 2) % m
        if i == 1:
            result = result * b % m
    return result

def expo_octal(b,e):
    e_bin = dec2bin(e)
    e_bin.reverse()
    if len(e_bin) % 3 == 1:
        e_bin = [0, 0] + e_bin
    elif len(e_bin) % 3 == 2:
        e_bin = [0] + e_bin
    result = 0
    if e_bin[0:3] == [0,0,0]:
        result = 1
    elif e_bin[0:3] == [0,0,1]:
        result = b
    elif e_bin[0:3] == [0,1,0]:
        result = b ** 2
    elif e_bin[0:3] == [0,1,1]:
        result = b ** 3
    elif e_bin[0:3] == [1,0,0]:
        result = b ** 4
    elif e_bin[0:3] == [1,0,1]:
        result = b ** 5
    elif e_bin[0:3] == [1,1,0]:
        result = b ** 6
    elif e_bin[0:3] == [1,1,1]:
        result = b ** 7
    else:
        pass
    for i in range(3, len(e_bin), 3):
        if e_bin[i:i+3] == [0,0,0]:
            result = result ** 8
        elif e_bin[i:i+3] == [0,0,1]:
            result = result ** 8 * b
        elif e_bin[i:i+3] == [0,1,0]:
            result = result ** 8 * b ** 2
        elif e_bin[i:i+3] == [0,1,1]:
            result = result ** 8 * b ** 3
        elif e_bin[i:i+3] == [1,0,0]:
            result = result ** 8 * b ** 4
        elif e_bin[i:i+3] == [1,0,1]:
            result = result ** 8 * b ** 5
        elif e_bin[i:i+3] == [1,1,0]:
            result = result ** 8 * b ** 6
        elif e_bin[i:i+3] == [1,1,1]:
            result = result ** 8 * b ** 7
    return result


def rtlbm_octal(b,e,m):
    """
    Return result for modular exponentiation binary with exponentiation by squaring octal method.
    https://en.wikipedia.org/wiki/Modular_exponentiation#Right-to-left_binary_method
    :param b: A number
    :type b: int
    :param e: A number
    :type e: int
    :param m: A number
    :type m: int
    :return: Return a number who is result modular exponentiation
    :rtype: int

    :Example:
    >>> rtlbm_octal(45,958,43)
    21
    """
    result = 1
    while e > 0:
        if e & 1 > 0:
            result = (result * b) % m
        e >>= 1
        b = expo_octal(b, 2) % m
    return result

def ltrbm_octal(b,e,m):
    """
    Return result for modular exponentiation binary with exponentiation by squaring octal method.
    https://en.wikipedia.org/wiki/Modular_exponentiation#Left-to-right_binary_method
    :param b: A number
    :type b: int
    :param e: A number
    :type e: int
    :param m: A number
    :type m: int
    :return: Return a number who is result modular exponentiation
    :rtype: int

    :Example:
    >>> ltrbm_octal(45,958,43)
    21
    """
    e_bin = dec2bin(e)
    result = 1
    e_bin.reverse()
    for i in e_bin:
        result = expo_octal(result, 2) % m
        if i == 1:
            result = result * b % m
    return result
