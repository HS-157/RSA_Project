#include <stdio.h>

long long expo_mod(long long b, long long e, long long m){
	long long f = 0;
	long long c = 1;
	long long tmp;
	while(f != e) {
		f++;
		c = (b * c) % m;
	}
	return c;
}

long long rtlbm(long long b, long long e, long long m) {
	long long result = 1;

	while(e > 0) {
		if (e & 1 > 0) result = (result * b) % m;
		e >>= 1;
		b = (b * b) % m;
	}
	return result;
}
