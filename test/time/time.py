from functools import partial
from timeit import *
from math import sqrt
from random import randint

from json_tools import write_json, read_json
from src.rsa.main import rsa

crypt_method = ["rsa.crypt_msg()", "rsa.crypt_msg_improve()", "rsa.crypt_msg_rewrite()"] + ["rsa.crypt_msg_rewrite" + str(i)  + "()" for i in range(2,12)]
decrypt_method = ["rsa.decrypt_msg()", "rsa.decrypt_msg_improve()", "rsa.decrypt_msg_rewrite()"] + ["rsa.decrypt_msg_rewrite" + str(i)  + "()" for i in range(2,12)]



number = 1
long_time = 10

rsa = rsa()

def a(integer):
    b = randint(5,integer - 3)
    c = integer - b
    return (b, c)

def size_bit(size):
    n,b = a(size)
    m = 0
    c = 0
    while(m < 3 and c < 3):
        m = randint(2 ** (n - 1), 2 ** n + 1)
        c = randint(2 ** (b - 1), 2 ** b + 1)
    mc = m * c
    if(mc.bit_length() != size):
        m,c = size_bit(size)
    return (m,c)

def b(num):
    a,b = size_bit(num)
    g = a * b
    a_len = a.bit_length()
    b_len = b.bit_length()
    g_len = g.bit_length()
    print("Size p :", a_len)
    print("Size q :", b_len)
    print("len :", a_len + b_len)
    print("Size p*q :", g_len)


def test_time(crypt_method, decrypt_method):
    
    for i in range(len(crypt_method)):
        time_list = []
        for k in range(10, 2 ** 12 + 1):
            m,p = size_bit(k)
            gf = m * p
            print("====================== Taille P*Q :",gf.bit_length(),"======================")
            rsa.get_keys(m, p)
            try:
                time = timeit(crypt_method[i], setup="from __main__ import rsa", number=number)
                time += timeit(decrypt_method[i], setup="from __main__ import rsa", number=number)
                print(crypt_method[i], ":", time)
                time_list.append([gf.bit_length(), time])
                write_json(crypt_method[i] + ".json", "time", time_list)
            except ValueError:
                break
            except:
                exit(0)
            if time > long_time:
                break

test_time(crypt_method, decrypt_method)