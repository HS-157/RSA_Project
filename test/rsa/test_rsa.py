import unittest
from src.convert import str2ascii, ascii2str
from src.bezout import bezout, xgcd
from src.prime import prime_with, prime
from src.rsa import keys, crypt, decrypt
from src.main import rsa
from time import sleep

class MyTest(unittest.TestCase):

    def setUp(self):
        self.keys = keys()
        self.public_key = self.keys[0]
        self.private_key = self.keys[1]
        self.msg = 'Hello World'
        self.msg_ascii = str2ascii(self.msg)
        self.msg_crypt = crypt(self.msg_ascii, self.public_key)
        self.rsa = rsa()

    def test_bezout(self):
        test = bezout(36, 69)
        self.assertIsInstance(test, int)
    
    def test_xgcd(self):
        test = xgcd(36, 98)
        self.assertIsInstance(test, tuple)
    
    def test_str2ascii(self):
        test = str2ascii("Hello World")
        self.assertIsInstance(test, list)
    
    def test_ascii2str(self):
        test = ascii2str([66, 111, 110, 106, 111, 117, 114])
        self.assertIsInstance(test, str)
    
    def test_prime_with(self):
        test = prime_with(69)
        self.assertIsInstance(test, int)

    def test_prime(self):
        test = prime(398)

    def test_keys(self):
        test = keys()
        self.assertIsInstance(test, tuple)
    
    def test_crypt(self):
        test = crypt(self.msg_ascii, self.public_key)
        self.assertIsInstance(test, list)
    
    def test_decrypt(self):
        test = decrypt(self.msg_crypt, self.private_key)
        self.assertIsInstance(test, list)

    def test_rsa_crypt_msg(self):
        self.rsa.crypt_msg()
        self.assertIsInstance(self.rsa.msg_crypt, list)

    def test_rsa_decrypt_msg(self):
        self.rsa.crypt_msg()
        self.rsa.decrypt_msg()
        self.assertIsInstance(self.rsa.msg_dcrpt, str)
    
    def test_rsa_keys(self):
        self.rsa.get_keys()
        self.assertIsInstance(self.rsa.keys, tuple)
    
    def test_rsa_crypt_msg_improve(self):
        self.rsa.crypt_msg_improve()
        self.assertIsInstance(self.rsa.msg_crypt, list)

    def test_rsa_decrypt_msg_improve(self):
        self.rsa.crypt_msg_improve()
        self.rsa.decrypt_msg_improve()
        self.assertIsInstance(self.rsa.msg_dcrpt, str)

    def test_rsa_crypt_msg_rewrite(self):
        self.rsa.crypt_msg_rewrite()
        self.assertIsInstance(self.rsa.msg_crypt, list)

    def test_rsa_decrypt_msg_rewrite(self):
        self.rsa.crypt_msg_rewrite()
        self.rsa.decrypt_msg_rewrite()
        self.assertIsInstance(self.rsa.msg_dcrpt, str)
    
    def test_rsa_crypt_msg_rewrite2(self):
        self.rsa.crypt_msg_rewrite2()
        self.assertIsInstance(self.rsa.msg_crypt, list)

    def test_rsa_decrypt_msg_rewrite2(self):
        self.rsa.crypt_msg_rewrite2()
        self.rsa.decrypt_msg_rewrite2()
        self.assertIsInstance(self.rsa.msg_dcrpt, str)
    
    def test_rsa_crypt_msg_rewrite3(self):
        self.rsa.crypt_msg_rewrite3()
        self.assertIsInstance(self.rsa.msg_crypt, list)

    def test_rsa_decrypt_msg_rewrite3(self):
        self.rsa.crypt_msg_rewrite3()
        self.rsa.decrypt_msg_rewrite3()
        self.assertIsInstance(self.rsa.msg_dcrpt, str)

    def test_rsa_crypt_msg_rewrite4(self):
        self.rsa.crypt_msg_rewrite4()
        self.assertIsInstance(self.rsa.msg_crypt, list)

    def test_rsa_decrypt_msg_rewrite4(self):
        self.rsa.crypt_msg_rewrite4()
        self.rsa.decrypt_msg_rewrite4()
        self.assertIsInstance(self.rsa.msg_dcrpt, str)

    def test_rsa_crypt_msg_rewrite5(self):
        self.rsa.crypt_msg_rewrite5()
        self.assertIsInstance(self.rsa.msg_crypt, list)

    def test_rsa_decrypt_msg_rewrite5(self):
        self.rsa.crypt_msg_rewrite5()
        self.rsa.decrypt_msg_rewrite5()
        self.assertIsInstance(self.rsa.msg_dcrpt, str)

    def test_rsa_crypt_msg_rewrite6(self):
        self.rsa.crypt_msg_rewrite6()
        self.assertIsInstance(self.rsa.msg_crypt, list)

    def test_rsa_decrypt_msg_rewrite6(self):
        self.rsa.crypt_msg_rewrite6()
        self.rsa.decrypt_msg_rewrite6()
        self.assertIsInstance(self.rsa.msg_dcrpt, str)

    def test_rsa_crypt_msg_rewrite7(self):
        self.rsa.crypt_msg_rewrite7()
        self.assertIsInstance(self.rsa.msg_crypt, list)

    def test_rsa_decrypt_msg_rewrite7(self):
        self.rsa.crypt_msg_rewrite7()
        self.rsa.decrypt_msg_rewrite7()
        self.assertIsInstance(self.rsa.msg_dcrpt, str)

    def test_rsa_crypt_msg_rewrite8(self):
        self.rsa.crypt_msg_rewrite8()
        self.assertIsInstance(self.rsa.msg_crypt, list)

    def test_rsa_decrypt_msg_rewrite8(self):
        self.rsa.crypt_msg_rewrite8()
        self.rsa.decrypt_msg_rewrite8()
        self.assertIsInstance(self.rsa.msg_dcrpt, str)

    def test_rsa_crypt_msg_rewrite9(self):
        self.rsa.crypt_msg_rewrite8()
        self.assertIsInstance(self.rsa.msg_crypt, list)

    def test_rsa_decrypt_msg_rewrite9(self):
        self.rsa.crypt_msg_rewrite8()
        self.rsa.decrypt_msg_rewrite8()
        self.assertIsInstance(self.rsa.msg_dcrpt, str)

    def test_rsa_crypt_msg_rewrite10(self):
        self.rsa.crypt_msg_rewrite10()
        self.assertIsInstance(self.rsa.msg_crypt, list)

    def test_rsa_decrypt_msg_rewrite10(self):
        self.rsa.crypt_msg_rewrite10()
        self.rsa.decrypt_msg_rewrite10()
        self.assertIsInstance(self.rsa.msg_dcrpt, str)

    def test_rsa_crypt_msg_rewrite11(self):
        self.rsa.crypt_msg_rewrite11()
        self.assertIsInstance(self.rsa.msg_crypt, list)

    def test_rsa_decrypt_msg_rewrite11(self):
        self.rsa.crypt_msg_rewrite11()
        self.rsa.decrypt_msg_rewrite11()
        self.assertIsInstance(self.rsa.msg_dcrpt, str)

if __name__ == '__main__':
    unittest.main()
