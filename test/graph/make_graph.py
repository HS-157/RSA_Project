from json_tools import read_json
from pylab import *


p = read_json("rsa.crypt_msg_rewrite().json", "time")
l = read_json("rsa.crypt_msg_rewrite2().json", "time")
m = read_json("rsa.crypt_msg_rewrite3().json", "time")
o = read_json("rsa.crypt_msg_rewrite4().json", "time")

list_fonction = ["rsa.crypt_msg()", "rsa.crypt_msg_improve()", "rsa.crypt_msg_rewrite()"] + ["rsa.crypt_msg_rewrite" + str(i)  + "()" for i in range(2,12)]

list_color = ["#4682B4", "#008000", "#00FF00", "#808080", "#000000", "#808000", "#008080", "#0000FF", "#000080", "#800080", "#FF00FF", "#FF4500", "#7B68EE"]

list_label = ["Modulus",
                "Python pow()",
                "Memory-efficient",
                "Memory-efficient C",
                "RtLb",
                "RtLb C",
                "LtRb",
                "RtLb squaring",
                "LtRb squaring",
                "RtLb squaring quater",
                "LtRb squaring quater",
                "RtLb squaring octal",
                "LtRb squaring octal"
            ]

def draw_json(label, color, value_list):
    value = read_json(value_list + ".json", "time")
    x = []
    y = []
    for i in value:
        x.append(i[0])
        y.append(i[1])
    plot(x, y, color=color, linewidth = 1, linestyle = "-", label = label, fillstyle = "full")#, marker = ".")

for i in range(0,len(list_fonction)):
    draw_json(list_label[i], list_color[i], list_fonction[i])

# On veut un grid
grid()
# La légende
legend(loc='upper center')
plt.xlabel('Size (bits)')
plt.ylabel('Time (s)')

# On affiche
show()

"""
def draw_eratosthenes(value):
    x = []
    y = []
    for i in value:
        x.append(i[0])
        y.append(i[1])
    plot(x, y, color="red", linewidth = 0.5, linestyle = "-", label = 'eratosthenes', fillstyle = "full", marker = ".")


def draw_eratosthenes_improve(value):
    x = []
    y = []
    for i in value:
        x.append(i[0])
        y.append(i[1])
    plot(x, y, color="blue", linewidth = 0.5, linestyle = "-", label = 'eratosthenes_improve', fillstyle = "full", marker = ".")

def draw_eratosthenes_ref(value):
    x = []
    y = []
    for i in value:
        x.append(i[0])
        y.append(i[1])
    plot(x, y, color="grey", linewidth = 0.5, linestyle = "-", label = 'eratosthenes_ref', fillstyle = "full", marker = ".")

def draw_atkin(value):
    x = []
    y = []
    for i in value:
        x.append(i[0])
        y.append(i[1])
    plot(x, y, color="green", linewidth = 0.5, linestyle = "-", label = 'atkin', fillstyle = "full", marker = ".")


draw_eratosthenes(p)
draw_eratosthenes_improve(l)
draw_eratosthenes_ref(m)
draw_atkin(o)


# On veut un grid
grid()
# La légende
legend(loc='upper center')
plt.xlabel('Number')
plt.ylabel('Time (s)')

# On affiche
show()
"""