# Import des modules
import json

# Nom du fichier
#json_file = "data.json"

def write_json(json_file, arg, val):
    data = {}
    data[arg] = val

    with open(json_file, 'w') as f:
        json.dump(data, f)


def read_json(json_file, arg):
    with open(json_file, 'r') as f:
        data = json.load(f)

    return data.get(arg)