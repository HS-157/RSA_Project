import unittest
from src.modular_exponentiation import mod_expo, mod_expo_c, rtlbm, rtlbm_c, rtlbm_ary, ltrbm_ary, ltrbm, expo_fast, expo_quater, rtlbm_quater, ltrbm_quater, expo_octal, rtlbm_octal, ltrbm_octal
from random import randint

class MyTest(unittest.TestCase):

    def test_mod_expo_c(self):
        b = randint(2**2,2**20)
        e = randint(2**2,2**20)
        m = randint(2**2,2**20)

        test = mod_expo_c(b,e,m)
        self.assertEqual(test, pow(b,e,m))

    def test_rtlbm(self):
        b = randint(2**2,2**20)
        e = randint(2**2,2**20)
        m = randint(2**2,2**20)

        test = rtlbm(b,e,m)
        self.assertEqual(test, pow(b,e,m))

    def test_mod_expo(self):
        b = randint(2**2,2**20)
        e = randint(2**2,2**20)
        m = randint(2**2,2**20)

        test = mod_expo(b,e,m)
        self.assertEqual(test, pow(b,e,m))

    def test_rtlbm_c(self):
        b = randint(2**2,2**20)
        e = randint(2**2,2**20)
        m = randint(2**2,2**20)

        test = rtlbm_c(b,e,m)
        self.assertEqual(test, pow(b,e,m))

    def test_ltrbm(self):
        b = randint(2**2,2**20)
        e = randint(2**2,2**20)
        m = randint(2**2,2**20)

        test = ltrbm(b,e,m)
        self.assertEqual(test, pow(b,e,m))

    def test_rtlbm_ary(self):
        b = randint(2**2,2**20)
        e = randint(2**2,2**20)
        m = randint(2**2,2**20)

        test = rtlbm_ary(b,e,m)
        self.assertEqual(test, pow(b,e,m))

    def test_ltrbm_ary(self):
        b = randint(2**2,2**20)
        e = randint(2**2,2**20)
        m = randint(2**2,2**20)

        test = ltrbm_ary(b,e,m)
        self.assertEqual(test, pow(b,e,m))

    def test_expo_fast(self):
        b = randint(2**2,2**20)
        e = randint(2**2,2**20)

        test = expo_fast(b,e)
        self.assertEqual(test, pow(b,e))

    def test_expo_quater(self):
        b = randint(2**2,2**20)
        e = randint(2**2,2**20)

        test = expo_quater(b,e)
        self.assertEqual(test, pow(b,e))

    def test_rtlbm_quater(self):
        b = randint(2**2,2**20)
        e = randint(2**2,2**20)
        m = randint(2**2,2**20)

        test = rtlbm_quater(b,e,m)
        self.assertEqual(test, pow(b,e,m))

    def test_ltrbm_quater(self):
        b = randint(2**2,2**20)
        e = randint(2**2,2**20)
        m = randint(2**2,2**20)

        test = ltrbm_quater(b,e,m)
        self.assertEqual(test, pow(b,e,m))

    def test_expo_octal(self):
        b = randint(2**2,2**20)
        e = randint(2**2,2**20)

        test = expo_octal(b,e)
        self.assertEqual(test, pow(b,e))

    def test_rtlbm_octal(self):
        b = randint(2**2,2**20)
        e = randint(2**2,2**20)
        m = randint(2**2,2**20)

        test = rtlbm_octal(b,e,m)
        self.assertEqual(test, pow(b,e,m))

    def test_ltrbm_octal(self):
        b = randint(2**2,2**20)
        e = randint(2**2,2**20)
        m = randint(2**2,2**20)

        test = ltrbm_octal(b,e,m)
        self.assertEqual(test, pow(b,e,m))

if __name__ == '__main__':
    unittest.main()
