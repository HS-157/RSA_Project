# diaporama readme 

##       @copyright      2016  Brest Open Campus EPSI


> Required (on linux)
* texlive-full

> To compile 
* open a terminal in the location of the makefile
* make clean && make

> Folder structure :  
* bibliography: location of the bib file containing bibliographical references
* font: Brest Open Campus corporate font
* images: pictures location
* package: location of style files
* tex: location of all .tex files
